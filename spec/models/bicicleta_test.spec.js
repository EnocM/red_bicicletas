var mongoose = require('mongoose');
const bicicleta = require('../../models/bicicleta');
var Bicicleta = require('../../models/bicicleta');

describe('Testing bicicletas', function () {
    beforeAll(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('We are connected to test database');
            done();
        });
    });

    afterAll(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "playera", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("playera");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        });

    
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            var oneBici = new Bicicleta({code: 10, color: "negro", modelo: "urbana"});
            Bicicleta.add(oneBici, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(oneBici.code);

                    done();
                });
            });
        });
    });
    

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(1);

                var aBici = new bicicleta({code: 1, color: "rojo", modelo: "playera"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "blanca", modelo: "1/2 carrera"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(2, function (error, targetBici){
                            expect(targetBici.code).toBe(aBici2.code);
                            expect(targetBici.color).toBe(aBici2.color);
                            expect(targetBici.modelo).toBe(aBici2.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });

});


// beforeEach(() => { Bicicleta.allBicis = [] });

// describe('Bicicleta.allBicis', () => {
//     it('comienza vacia', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add', () => {
//     it('agregamos una bici', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var a = new Bicicleta(1, 'azul', '1/2 carrera', [-34.6049932, -58.4367122, 21]);
//         Bicicleta.add(a);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);
//     });
// });

// describe('Bicicleta.findById', () => {
//     it('debe devolver la bici con id 2', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var b = new Bicicleta(1, "rojo", "Playera", [-34.5809316, -58.4204263, 21]);
//         var c = new Bicicleta(2, "negro", "Montaña", [-34.5930425, -58.419088]);
//         Bicicleta.add(b);
//         Bicicleta.add(c);

//         var targetBici = Bicicleta.findById(2);

//         expect(targetBici.id).toBe(2);
//         expect(targetBici.color).toBe(c.color);
//     });
// });

// describe('Bicicleta.removeById', () => {
//     it('debe eliminar la bici con id 2', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var a = new Bicicleta(1, 'azul', '1/2 carrera', [-34.6049932, -58.4367122, 21]);
//         var b = new Bicicleta(2, "rojo", "Playera", [-34.5809316, -58.4204263, 21]);
//         var c = new Bicicleta(3, "negro", "Montaña", [-34.5930425, -58.419088]);    
//         Bicicleta.add(a);
//         Bicicleta.add(b);
//         Bicicleta.add(c);

//         Bicicleta.removeById(2);

//         expect(Bicicleta.allBicis.length).toBe(2);
//         expect(Bicicleta.allBicis[0].id).toBe(1);
//         expect(Bicicleta.allBicis[1].id).toBe(3);
//     });
// });